#!/bin/python3

import requests as rq
import time
from random import randrange
from datetime import datetime
from bs4 import BeautifulSoup as bs

import re, logging

from multiprocessing.pool import Pool

from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base

from proxy_list import Proxy, proxis

#from os import nice
# ORM Part
class DB:
    def __init__(self,
                 user='rjw',
                 password="h5Zu%XJ",
                 db='art1',
                 server='localhost',
                 port='5432'):

        self.user = user
        self.db = db
        self.server = server
        self.port = port
        self.password = password
        super(DB, self).__init__()

    def connect(self):
        eng = create_engine("postgresql://{0}:{1}@{2}:{3}/{4}".format(
            self.user,
            self.password,
            self.server,
            self.port,
            self.db
            ))
        if not eng.dialect.has_table(eng, 'sales'):
            meta = MetaData(eng)
            Table('sales', meta,
                  Column('id', Integer(), primary_key=True, nullable=False),
                  Column('title', String),
                  Column('estimate', Float),
                  Column('sellPrice', Float),
                  Column('saleDate', Date),
                  Column('writeDate', Date)
                  )
            meta.create_all()

        if not eng.dialect.has_table(eng, 'prox'):
            meta = MetaData(eng)
            Table('prox', meta,
                  Column('id', Integer(), primary_key=True, nullable=False),
                  Column('url', String),
                  Column('prot', String),
                  Column('state', String),
                  )
            meta.create_all()

        return sessionmaker(bind=eng)

DECDB = declarative_base()


def openSession():
    return DB().connect()()

class Sales(DECDB):
    __tablename__ = 'sales'
    id = Column(Integer(), primary_key=True)
    title = Column(String)
    estimate = Column(Float)
    sellPrice = Column(Float, default=0.0)
    saleDate = Column(Date)
    writeDate = Column(Date, default=datetime.now())

    def __init__(self, title, estimate, salePrice, saleDate):
        self.title = title
        self.estimate = estimate
        self.sellPrice = salePrice
        self.saleDate = saleDate

    def __eq__(self, other):
        return self.title == other.title and \
        self.estimate == other.estimate and \
        self.sellPrice == other.sellPrice and \
        self.saleDate == other.saleDate


class Prox(DECDB):
    __tablename__ = 'prox'
    id = Column(Integer(), primary_key=True)
    url = Column(String)
    prot = Column(String)
    state = Column(String)

    def __init__(self, url, prot, state):
        self.url = url
        self.prot = prot
        self.state = state
    def getUrl(self):
        return "{0}://{1}".format(
            self.prot.lower(),
            self.prot.url())
    def checkProxy(self):
        res = rq.head(self.getUrl, timeout=8)\
                   .status_code == 200
        return res

    def convertFromProxy(self, proxy:Proxy):
        state = 'active' if self.checkProxy() else "inactive"
        return Prox(url=proxy.prox,
                    prot=proxy.prot,
                    state=state)

    def read(self, val:dict):
        session = openSession()
        res = []
        try:
            if not val or val == {}:
                res = session.query(Prox).all()
            else:
                condition = (lambda : Prox.id == val.get('id') if val.get('id') else True) and \
                            (lambda : Prox.url == val.get('url') if val.get('url') else True) and \
                            (lambda : Prox.prot == val.get('prot') if val.get('prot') else True) and \
                            (lambda : Prox.state == val.get('state') if val.get('state') else True)
                res = session.query(Prox).filter_by(condition).all()
        except:
            session.rollback()
            return False
        session.close()
        return res

# ~ORM PART
dbname = 'art1'
pagesRange = (1, 1500)
numberOfFlows = 4

def getProxy():
    session = openSession()
    proxyList = session.query(Prox).filter_by(state='active').all()
    p = {}
    if proxyList and len(proxyList) > 0:
        p = proxyList[randrange(0, len(proxyList))]
        p = {p.prot: p.url}
    session.close()
    return p

def getFloat(string):
    res = 0.0
    try:
        res = (re.search('(\d+[\.|\,]\d*){1}', string)).group(1)
    except:
        pass
    return float(res) if res else 0


def parseTask(page,iterator=0):
    session = openSession()
    proxy =  getProxy() # if iterator < 48 else {}
    url = "http://adelanta.biz/archive-sdelok/?PAGEN_1={0}" \
               .format(page)
    print(url)
    print(proxy)
    res = True
    try:
        time.sleep(1.3)
        try:
            request = rq.get(url,
                              proxies=proxy,
                              headers={
                                  "User-Agent": "Mozilla/5.0 (Windows 13) \
                                  AppleWebKit/537.36 (KHTML, like Gecko) \
                                  Chrome/67 Safari/537.36",
                              },
                              timeout=30)
        except: 
            time.sleep(61)
            session.close()
            parseTask(page, iterator+1)
            return False 
        content = request.content
        time.sleep(0.1)
        pageDate = bs(content, "lxml")\
                .find("table", {"class": "deal-list"})
        if not pageDate:
            time.sleep(100)
            return  parseTask(page,iterator = iterator + 1)
        elif not pageDate and iterator > 10:
            return parseTask(page,iterator = iterator + 1)
        newData = [dt for dt in \
                   [[re.sub('(\n)|(\t)','', item.text) \
                    for item in row.findAll('td')] \
                    for row in pageDate.findAll('tr') if row]\
                   if len(dt) > 0]
        for item in newData:
            if len(item) > 0:
                sale = Sales(title=str(item[0]),
                             estimate = getFloat(item[1]),
                             salePrice = getFloat(item[2]),
                             saleDate = None if item[3] == '�' else \
                             datetime.strptime(re.sub('\s(\d{2}(\:*)){3}', '',item[3]), "%d.%m.%Y"))
                if (len(session.query(Sales).filter(
                        Sales.title == sale.title and \
                        Sales.estimate == sale.estimate and \
                        Sales.salePrice == sale.salePrice and \
                        Sales.saleDate == sale.saleDate).all()) == 0):
                    session.add(sale)
                    session.commit()
    except:
        time.sleep(1)
        if iterator < 3:
            proxy = session.query(Prox)\
                .filter_by(prot=list(proxy.keys())[0],
                           url=proxy[proxy.keys[0]]).first()
            if not proxy.check():
                proxy.state = "inactive"
                session.commit()
        print ("Cached Exception: {0}".format(url))
        if iterator > 15:
            time.sleep(65)
        elif iterator > 25:
            time.sleep(120)
        elif iterator > 55:
            logging.error("link {0} with do not responce"\
                                 .format(url))
        res = False
    finally:
        session.close()
        if not res:
            return parseTask(page,iterator=iterator+1)
    return res


def getWorkingProxies(proxies = proxis):
    session = openSession()
    total = len(proxies)
    active = session.query(Prox).filter(Prox.state == 'active').all()
    for i,item in enumerate(proxies):
        try:
            assert(rq.head(url=item.getUrl(), timeout=7).status_code == 200)

            validation = session.query(Prox) \
                .filter(Prox.url == item.prox \
                        and Prox.prot == item.prot).all()
            if len(validation) == 0:
                prox = Prox(url=item.prox, prot=item.prot, state="active")
                session.add(prox)
                session.commit()
                print('proxy: {0} added'.format(item.getUrl()))

        except:
            print('loaded {0} from {1} successed {2}'.format(i,total, len(active)))

        finally:
            session.close()
    return True


previousState = 0

if __name__ == "__main__":
    pool_ = Pool(processes=numberOfFlows)
    taskas = pool_.map_async(parseTask, range(pagesRange[0], pagesRange[1]))
    taskas.get()
